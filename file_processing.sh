#!/bin/bash

# Sprawdzanie czy istnieje ścieżka
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <directory_path>"
    exit 1
fi

directory_path="$1"

# Lista plików
echo "List of files:"
find "$directory_path" -type f
echo

# Liczenie plików
num_files=$(find "$directory_path" -type f | wc -l)
echo "Number of files: $num_files"
echo

# Liczenie folderów
num_directories=$(find "$directory_path" -type d | wc -l)
echo "Number of directories: $num_directories"
echo

# szuka keyworda
read -p "Enter the keyword to search for: " keyword
echo "Keyword \"$keyword\" found in:"
grep -rli "$keyword" "$directory_path"
echo

# sprawdzenie + backup
backup_dir="/home/kacpiwal/backup"
if [ ! -d "$backup_dir" ]; then
    mkdir "$backup_dir"
fi
cp -r "$directory_path"/* "$backup_dir"/
echo "Backup directory created at: $backup_dir"
echo


echo "Summary:"
echo "- Total files: $num_files"
echo "- Total directories: $num_directories"
num_keyword_files=$(grep -rli "$keyword" "$directory_path" | wc -l)
echo "- Keyword \"$keyword\" found in $num_keyword_files files"

#dodalem i zeby szukalo ZNAJDZ i ZnajdZ
