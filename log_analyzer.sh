#!/bin/bash

# jesli puste
if [ "$#" -ne 1 ]; then
    echo "How to use: $0 <log_path>"
    exit 1
fi

log_file="$1"

if [ ! -f "$log_file" ] || [ ! -r "$log_file" ]; then
    echo "The log file doesn't exist or it's not readable"
    exit 1
fi

total_lines=$(wc -l < "$log_file")

# sprawdzanie errorow i warningow

error_lines=$(grep -i "error" "$log_file" | wc -l)
warning_lines=$(grep -i "warning" "$log_file" | wc -l)

# unikatowe ip

unique_adressess=$(awk '{print $1}' "$log_file" | sort | uniq)

# total unikatowe ip

total_unique=$(awk '{print $1}' "$log_file" | sort | uniq | wc -l)

# top 5

top_5=$(awk '{print $6, $7}' "$log_file" | sort | uniq -c | sort -nr | head -n 5 | awk '{print NR".", $2, $3, "-", $1, "times"}')


report="/home/kacpiwal/reporting/log_analysis_file.txt"
{
    echo "Log Analysis Report (Generated on: $(date))"
    echo
    echo "Total lines in log: $total_lines"
    echo
    echo "Error lines: $error_lines"
    echo "Warning lines: $warning_lines"
    echo "Unique IP Adressess:"
    echo "$unique_adressess"
    echo
    echo "Total unique IP adressess: $total_unique"
    echo
    echo "Top 5 Most Frequent Entries:"
    echo "$top_5"

} > "$report"
